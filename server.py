import base64
import hmac
import hashlib
import json

from fastapi import FastAPI, Form, Cookie, Body, datastructures
from fastapi import responses
from fastapi.responses import Response
from typing import Optional # for cookie


app = FastAPI()


users = {
    'ivan@user.com': {
        'name': 'Ivan',
        'password': '7c49fc58ba0c39e3a4f6843b3bfbb7d9943c8552387d2f841f92cafb5d77dd73',
        'balance': 100000
    },
    'petr@user.com': {
        'name': 'Petr', 
        'password': 'ae0ebb83eaa46eecec4113fc957ed3c6115d337395ed3c56d67007b8bf39c90e',
        'balance': 50000
    }
}

# openssl rand -hex 32
SECRET_KEY = 'e923a3575ca43a88ca63701d602197a5dd74da6f79d2d49edf011b9070d11110'
PASSWORD_SALT = '5f895dc65e17a5c94f3e4a96ba2d70396dcc5fa17307ad32681b12f9b33b4a81'

def sign_data(data: str) -> str:
    '''return sign data'''
    return hmac.new(SECRET_KEY.encode(), msg=data.encode(), digestmod=hashlib.sha256).hexdigest().upper()

def get_username_from_signed_string(username_signed: str) -> Optional[str]:
    """Get email"""
    username_base64, sign = username_signed.split('.')
    username = base64.b64decode(username_base64.encode()).decode()
    valid_sign = sign_data(username)
    # if valid_sign == sign:
    if hmac.compare_digest(valid_sign, sign):
        return username


def verify_password(password: str, password_hash: str) -> bool:
    return hashlib.sha256((password + PASSWORD_SALT).encode()).hexdigest().lower() \
           == password_hash.lower()



@app.get('/')
def index_page(username: Optional[str] = Cookie(default=None)): # Check cookies
    with open('templates/index.html', 'r') as f:
        login_page = f.read()
    if not username:
        return Response(login_page.encode('utf-8'))

    valid_usename = get_username_from_signed_string(username)
    if not valid_usename:
        response = Response(login_page.encode('utf-8'))
        response.delete_cookie(key='username')
        return response


    return Response(f'Hello {users[valid_usename]["name"]}!')



@app.post('/login')
# def login_page(data: dict = Body(...)):
#     print(data)
    # username = data['username']
    # password = data['password']

def login_page(username : str = Form(...), password : str = Form(...)): # использование FormData
    user = users.get(username) # 'ivan@user.com': {'name': 'Ivan', 'password': '12345', 'balance': 100000}
    # print(user)
    if not user or not verify_password(password, user['password']):
        return Response(
            json.dumps({
                'success': False,
                'message': 'I dont you!'

            }),
            media_type='application/json')
    
    response = Response(
            json.dumps({
            'success': True, 
            'message': f'Привет, {user["name"]}, твой баланс: {user["balance"]}'
            }),
            media_type='application/json')

    username_signed = base64.b64encode(username.encode()).decode() + '.' + sign_data(username)
    response.set_cookie(key='username', value=username_signed) # username="ivan@user.com"
    return response