Authentication Fastapi
start:
- pip install -r requirements.txt
- uvicorn server:app --reload --host 0.0.0.0
